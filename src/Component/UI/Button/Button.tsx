import React, { FC } from 'react'
import { Props } from "./types";
import c from 'classnames'
import './Button.scss'

const Button: FC<Props> = ({ title, customClass, onClick }) => {
    return(
        <button
            onClick={onClick}
            className={c(customClass, 'MainButton')}
        >
            {title}
        </button>

    )
}
export default Button;