import React, { FC } from 'react'
import { Props } from './types'

import './Input.scss'

const Input: FC<Props> = props => {
    const {type , placeholder, defaultValue } = props
    return(
        // FIXME: Make alignment better. If you use VSCode, you can set it up to align the code on every file save
        <input type={type} placeholder={placeholder} value={defaultValue} />
        // FIXED

    )
}
export default Input;