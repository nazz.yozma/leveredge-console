export type Props = {
    // TODO: Should all of these be optionals? By default, everything should be required, 
    // and only if it's necessary, we make it optional
    type: string,
    placeholder: string,
    defaultValue?: string;
}