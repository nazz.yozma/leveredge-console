import React, {FC} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { routes } from "./modules/routes/routes";
import store from "./Store/Store";


// Should be typed (React.FC)
const WrapperRouter:FC = () => {
    return(
        <Router>
            <Switch>
                {routes.map( (route) => (
                    <Route key={route.path} {...route} />
                )) }
            </Switch>
        </Router>
    )
}

ReactDOM.render(
    <Provider store={store}>
       <WrapperRouter />
    </Provider>,
  document.getElementById('root')
);






