import React from 'react'
import { Link } from "react-router-dom";
import NeedHelpButton from "./NeedHelpButton/NeedHelpButton";
import { Input, Button } from "../../Component";

import './Login.scss'
import Logo from '../../assets/image/Logo.png'

const Login = () => {
    return (
        <div className="Login">
            <Link to='/'>
                <img src={Logo} alt="logo"/>
            </Link>

{/* TODO: Going forward, I'd recommend you to use explicit strings all the time */}
{/* meaning  `Change DNS records for` becomes {'Change DNS records for'} */}
            <h1>{'Change DNS records for besman.co.il'}</h1>
            <p>{'In order to activate the service and optimize tour website. It\'s neccessary to follow the instructions.'}</p>

            <div className="Login-Form">
                <div className="Login-Form__Steps">
                    <h3>{'1. Login to your hosting / domain account:'}</h3>
                    <span>{'Your DNS is managed by: Hostgatar.com'}</span>
                </div>
                <div className="Login-Form__Steps">
                    <h3>{'2. Change current CNAME:'}</h3>
                    <div className="Login-Form__Steps-Item">
                        <Input type="text" placeholder="www.besman.co.il" />

                        <div className="hr"/>

                        <Input type="text" placeholder="click to copy"/>
                    </div>
                </div>
                <div className="Login-Form__Steps">
                    <h3>{'3. Change current A record:'}</h3>
                    <div className="Login-Form__Steps-Item">
                        <Input type="text" placeholder="besman.co.il"/>
                        <div className="hr"/>
                        <Input type="text" placeholder="10.20.20.1"/>
                    </div>
                </div>
                <div className="Login-Form__Confirm">
                    <Button title="Done. Verify Changes" customClass="ConfirmButton"
                    />
                </div>

                <NeedHelpButton/>
            </div>
        </div>
    )
}
export default Login;