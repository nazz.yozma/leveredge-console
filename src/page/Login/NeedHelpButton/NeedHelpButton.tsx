import React from 'react'
import Button from "../../../Component/UI/Button/Button";

import './NeedHelpButton.scss'
// FIXME: Fix alignment
const NeedHelpButton = () => {
    return(
        <div className="NeedHelpButton">
            <h3>{'Need help ?'}</h3>

            <div className="NeedHelpButton-Row">
                <Button title="Chat with support" customClass="HelpButton" />
                <Button title="Send instructions to webmaster" customClass="HelpButton" />
            </div>
        </div>

    )
}
export default NeedHelpButton;